/*
 * grunt-init-standard
 *
 */

"use strict";

// Basic template description.
exports.description =
  "Create a commonjs module, including Nodeunit unit tests.";

// Template-specific notes to be displayed before question prompts.
exports.notes = "";

// Template-specific notes to be displayed after question prompts.
exports.after =
  "You should now install project dependencies with _npm " +
  "install_. After that, you may execute project tasks with _grunt_. For " +
  "more information about installing and configuring Grunt, please see " +
  "the Getting Started guide:" +
  "\n\n" +
  "http://gruntjs.com/getting-started";

// Any existing file or directory matching this wildcard will cause a warning.
exports.warnOn = "*";

exports.template = function(grunt, init, done) {
  init.process(
    {},
    [
      // Prompt for these values.
      init.prompt("name"),
      init.prompt("description"),
      init.prompt("version"),
      init.prompt("repository"),
      init.prompt("homepage"),
      init.prompt("licenses"),
      init.prompt("author_name"),
      init.prompt("author_email"),
      init.prompt("author_url"),
      init.prompt("node_version")
    ],
    function(err, props) {
      props.keywords = [];
      props.devDependencies = {
        glob: "^7.0.5",
        grunt: "^1.0.1",
        "grunt-autoprefixer": "^3.0.4",
        "grunt-browser-sync": "^2.2.0",
        "grunt-contrib-clean": "^1.0.0",
        "grunt-contrib-concat": "^1.0.1",
        "grunt-contrib-copy": "^1.0.0",
        "grunt-contrib-imagemin": "^1.0.1",
        "grunt-contrib-jshint": "^1.0.0",
        "grunt-contrib-uglify": "^1.0.1",
        "grunt-contrib-watch": "^1.0.0",
        "grunt-newer": "^1.2.0",
        "grunt-notify": "^0.4.5",
        "grunt-sass": "^1.2.1",
        hamburgers: "^0.9.3",
        "jit-grunt": "^0.10.0",
        jquery: "^3.3.1",
        "load-grunt-config": "^0.19.2",
        matchdep: "^1.0.1",
        "time-grunt": "^1.3.0"
      };

      //create directories
      var join = require("path").join;
      grunt.file.mkdir(join(init.destpath(), "src/assets/images/"));
      grunt.file.mkdir(join(init.destpath(), "src/assets/fonts/"));
      grunt.file.mkdir(
        join(init.destpath(), "src/assets/sass/components/mixins/")
      );
      grunt.file.mkdir(join(init.destpath(), "src/assets/svgs/"));

      // Files to copy (and process).
      var files = init.filesToCopy(props);

      // Actually copy (and process) files.
      init.copyAndProcess(files, props);

      // Generate package.json file.
      init.writePackageJSON("package.json", props);

      // All done!
      done();
    }
  );
};

module.exports = function(grunt) {
    var path = require('path');

 	// Make sure init has been run
	// if(!grunt.file.exists('vendor/bootstrap-sass') && (grunt.cli.tasks[0] != 'init')) {
	// 	grunt.fail.fatal('>> Please run "grunt init" before continuing.');
	// }
    
    var fileStatus = grunt.file.exists('_mapDrive.json');

	require('time-grunt')(grunt);

    require('load-grunt-config')(grunt, {
        // path to task.js files, defaults to grunt dir
        configPath: [
        	path.join(process.cwd(), 'grunt/config/'),
        	path.join(process.cwd(), 'grunt/tasks/')
        ],
		// overridePath: [
		// 	path.join(process.cwd(), 'variant-n')
		// ],

        // auto grunt.initConfig
        init: true,

        // data passed into config.  Can use with <%= test %>
        data: {
            pkg: grunt.file.readJSON('package.json'),
			config: grunt.file.readYAML('config.yml'),
            mapDrive: [fileStatus]
        },
        
		jitGrunt: {
      		customTasksDir: 'grunt/tasks',
            staticMappings: {
                includereplace: 'grunt-include-replace',
                usebanner: 'grunt-banner'
            }
    	}

    });

};
module.exports = {
  options: {
    includePaths: [
      "<%= config.assets %>sass/components/",
      "<%= config.vendor %>"
    ]
  },
  dev: {
    options: {
      sourceMap: true
    },
    files: {
      "<%= config.build_assets %>css/styles.css":
        "<%= config.assets %>sass/styles.scss"
    }
  },
  build: {
    options: {
      sourceMap: false,
      outputStyle: "compressed"
    },
    files: {
      "<%= config.build_assets %>css/styles.css":
        "<%= config.assets %>sass/styles.scss"
    }
  }
};

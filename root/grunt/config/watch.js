module.exports = {
  options: {
    livereload: true
  },
  sass: {
    files: ["<%= config.assets %>sass/**/*"],
    tasks: ["sass:dev", "autoprefixer:default", "notify:sass"],
    options: {
      spawn: false
    }
  },
  scripts: {
    files: ["<%= config.assets %>js/**/*.js", "<%= config.lib %>**/*.js"],
    tasks: ["concat:dev", "notify:scripts"],
    options: {
      spawn: false
    }
  },
  svgs: {
    files: ["<%= config.assets %>svgs/**/*.svg"],
    tasks: ["newer:copy:svgs", "notify:svgs"],
    options: {
      spawn: false
    }
  },
  images: {
    files: [
      "<%= config.assets %>images/**/*.*",
      "<%= config.assets %>dev_images/*.*"
    ],
    tasks: ["newer:copy:images", "notify:images"],
    options: {
      spawn: false
    }
  },
  fonts: {
    files: ["<%= config.assets %>fonts/**/*.{eot,svg,ttf,woff}"],
    tasks: ["newer:copy:fonts", "notify:fonts"],
    options: {
      spawn: false
    }
  },
  files: {
    files: ["<%= config.assets %>files/**/*.*"],
    tasks: ["newer:copy:files", "notify:files"],
    options: {
      spawn: false
    }
  },
  php: {
    files: ["<%= config.dev_html %>/**/*"],
    tasks: ["clean:html", "newer:copy:php", "notify:php"], //"includereplace:default", "notify:html"],
    options: {
      spawn: false
    }
  },
  ee: {
    files: ["<%= config.dev_ee_templates %>/**/*"],
    tasks: ["newer:copy:ee_templates", "notify:ee"],
    options: {
      spawn: false
    }
  }
};

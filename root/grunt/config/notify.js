module.exports = {
  php: {
    options: {
      message: "PHP Copied"
    }
  },
  images: {
    options: {
      message: "Images Copied"
    }
  },
  files: {
    options: {
      message: "Files Copied"
    }
  },
  scripts: {
    options: {
      message: "JS Copied"
    }
  },
  svgs: {
    options: {
      message: "SVGs Copied"
    }
  },
  fonts: {
    options: {
      message: "Fonts Copied"
    }
  },
  sass: {
    options: {
      message: "SASS Successfully Compiled"
    }
  },
  ee: {
    options: {
      message: "EE Copied"
    }
  }
};

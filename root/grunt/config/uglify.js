module.exports = {
  default: {
    files: {
      "<%= config.build_assets %>js/scripts.min.js": [".temp/js/scripts.min.js"]
    }
  }
};

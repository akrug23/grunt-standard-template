module.exports = {
  php: {
    files: [
      {
        expand: true,
        dot: true,
        cwd: "<%= config.dev_html %>",
        src: ["**/*.*"],
        dest: "<%= config.build_html %>"
      }
    ]
  },
  files: {
    files: [
      {
        expand: true,
        cwd: "<%= config.assets %>files/",
        src: ["**/*"],
        dest: "<%= config.build_assets %>files/"
      }
    ]
  },
  images: {
    files: [
      {
        expand: true,
        cwd: "<%= config.assets %>images/",
        src: ["**/*"],
        dest: "<%= config.build_assets %>images/"
      }
    ]
  },
  fonts: {
    files: [
      {
        expand: true,
        cwd: "<%= config.assets %>fonts/",
        src: ["**/*"],
        dest: "<%= config.build_assets %>fonts/"
      }
    ]
  },
  svgs: {
    files: [
      {
        expand: true,
        cwd: "<%= config.assets %>svgs/",
        src: ["**/*"],
        dest: "<%= config.build_assets %>svgs/"
      }
    ]
  },
  vendor_js: {
    files: [
      {
        expand: true,
        cwd: "<%= config.vendor %>",
        src: ["jquery/dist/jquery.min.js"],
        dest: "<%= config.build_assets %>js/vendor/"
      }
    ]
  },
  ee_templates: {
    files: [
      {
        dot: true,
        expand: true,
        cwd: "<%= config.dev_ee_templates %>",
        src: ["**/*"],
        dest: "<%= config.build_ee_templates %>"
      }
    ]
  }
};

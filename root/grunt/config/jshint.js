module.exports = {
	options: {
		jshintrc: true
	},
	build: [
		'<%= config.assets %>/js/**/*.js'
	] 
}
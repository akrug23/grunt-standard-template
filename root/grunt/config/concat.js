module.exports = {
  dev: {
    options: {
      sourceMap: true,
      separator: ";"
    },
    src: [
      "<%= config.assets %>js/marketo.js",
      "<%= config.assets %>js/custom.js",
      "<%= config.assets %>js/resize_debug.js"
    ],
    dest: "<%= config.build_assets %>js/scripts.min.js"
  },
  build: {
    options: {
      sourceMap: false,
      separator: ";"
    },
    src: [
      "<%= config.assets %>js/marketo.js",
      "<%= config.assets %>js/custom.js"
    ],
    dest: ".temp/js/scripts.min.js"
  }
};

module.exports = {
  html: ["<%= config.build_html %>**/*.*"],
  temp: [".temp/"],
  all: [
    "<%= config.build_html %>",
    "<%= config.build_assets %>",
    "<%= config.build_ee_templates %>"
  ]
};

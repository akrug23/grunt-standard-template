module.exports = {
  options: {
    map: true,
    browsers: ["last 10 version", "ie 9", "ie 10", "ie 11"]
  },
  default: {
    src: "<%= config.build_assets %>css/styles.css"
  }
};

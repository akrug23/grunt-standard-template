module.exports = {
  dev: {
    bsFiles: {
      src: ["<%= config.build_html %>**/*.*"]
    },
    options: {
      proxy: "<%= config.hostname %>.localhost",
      watchTask: true,
      open: true,
      tunnel: "<%= config.hostname %>"
    }
  }
};

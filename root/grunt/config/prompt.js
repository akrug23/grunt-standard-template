module.exports = function(grunt){
    return {
        init: {
            options: {
                questions: [
                    {
                        config: 'new_drive_settings', // arbitrary name or config for any other grunt task 
                        type: 'list', // list, checkbox, confirm, input, password 
                        message: 'Do you want to update/use map drive settings:', // Question to ask the user, function needs to return a string, 
                        choices: [
                            {
                                name: 'Yes',
                                value: true
                            },
                            {
                                name: 'No',
                                value: false
                            }
                        ]
                    },
                    {
                        config: 'json_generator.init.options.username', // arbitrary name or config for any other grunt task 
                        type: 'list', // list, checkbox, confirm, input, password 
                        message: 'Pick your name:', // Question to ask the user, function needs to return a string, 
                        choices: [
                            {
                                name: 'Aaron',
                                value: 'akrug'
                            },
                            {
                                name: 'Scott',
                                value: 'skeen'
                            }
                        ],
                        when: function(answers) {
                            if (typeof answers.new_drive_settings !== 'undefined'){
                                return answers.new_drive_settings;
                            } else {
                                return true;
                            }
                        }
                    },
                    {
                        config: 'json_generator.init.options.password', // arbitrary name or config for any other grunt task 
                        type: 'input', // list, checkbox, confirm, input, password 
                        message: 'Enter your password:', // Question to ask the user, function needs to return a string,
                        filter:  function(value){
                            return encodeURI(value);
                        },
                        when: function(answers) {
                            if (typeof answers.new_drive_settings !== 'undefined'){
                                return answers.new_drive_settings;
                            } else {
                                return true;
                            }
                        }
                    },
                    {
                        config: 'json_generator.init.options.shareHost', // arbitrary name or config for any other grunt task 
                        type: 'list', // list, checkbox, confirm, input, password 
                        message: 'Pick the development server:', // Question to ask the user, function needs to return a string, 
                        choices: [
                            {
                                name: 'trouble-a',
                                value: 'trouble-a-virginia529.vcsp.virginia529.com'
                            },
                            {
                                name: 'trouble-s',
                                value: 'trouble-s-virginia529.vcsp.virginia529.com'
                            }
                        ],
                        when: function(answers) {
                            if (typeof answers.new_drive_settings !== 'undefined'){
                                return answers.new_drive_settings;
                            } else {
                                return true;
                            }
                        }
                    },  
                    {
                        config: 'json_generator.init.options.shareFolder', // arbitrary name or config for any other grunt task 
                        type: 'input', // list, checkbox, confirm, input, password 
                        message: 'Enter share folder:', // Question to ask the user, function needs to return a string, 
                        default: 'wwwroot/',
                        when: function(answers) {
                            if (typeof answers.new_drive_settings !== 'undefined'){
                                return answers.new_drive_settings;
                            } else {
                                return true;
                            }
                        }
                    }
                ]
            }
        }
    }
}
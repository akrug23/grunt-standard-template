module.exports = {
  test: {
    options: {
      port: "8010",
      hostname: "127.0.0.1",
      open: false,
      keepalive: false,
      base: "<%= config.build_html %>"
    }
  }
};

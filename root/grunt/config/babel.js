module.exports = {
  options: {
    presets: ["env"]
  },
  dev: {
    options: {
      sourceMap: false
    },
    files: {
      "<%= config.build_assets %>js/app.js": "<%= config.assets %>js/custom.js"
    }
  },
  build: {
    options: {
      sourceMap: false
    },
    files: {
      "<%= config.build_assets %>js/app.js": "<%= config.assets %>js/custom.js"
    }
  }
};
